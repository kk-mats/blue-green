#!/usr/bin/env bash
set -Ceuo pipefail

export IMAGE_TAG="${CI_COMMIT_TAG:-CI_COMMIT_SHORT_SHA}"

function build() {
	mkdir -p /kaniko/.docker
	echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
	/kaniko/executor --context $CI_PROJECT_DIR \
		--dockerfile $CI_PROJECT_DIR/Dockerfile \
		--destination $CI_REGISTRY_IMAGE:$IMAGE_TAG \
		--build-arg "VERSION_TAG=$1"
}

function main() {
	case $1 in
		build ) build ${@:2} ;;
		* ) echo "Invalid arguments \"$@\"" && exit 1 ;;
	esac
}

main $@