FROM node:16-bullseye-slim
EXPOSE 3000
ARG VERSION_TAG
COPY src /bg
WORKDIR /bg
RUN	yarn install
ENTRYPOINT [ "yarn", "run", "start" ]